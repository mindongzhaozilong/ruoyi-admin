package com.ruoyi.learn;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author chenliang
 * @date 2023/12/17 9:43
 */
@FunctionalInterface
public interface DateToStringTranslater {
    String justOneMethod();
}
