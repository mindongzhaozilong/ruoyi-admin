package com.ruoyi.learn;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.data.convert.Jsr310Converters;

import com.fasterxml.jackson.datatype.jsr310.deser.InstantDeserializer;

/**
 * @author chenliang
 * @date 2023/12/17 9:36
 */
public class Test {
    @Deprecated
    public String test(){
        System.out.println("1111");
        return "111";

    }

    public static void main(String[] args) {
        Test test = new Test();
        test.test();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String stringDate = test.getStringDate(() -> simpleDateFormat.format(new Date()));
        System.out.println(stringDate);
    }

    @SuppressWarnings(value = "all")
    public void unusedMethod(){
        System.out.println("没被使用过的方法");
    }

    public String getStringDate(DateToStringTranslater dateToStringTranslater){
        return dateToStringTranslater.justOneMethod();
    }
}
