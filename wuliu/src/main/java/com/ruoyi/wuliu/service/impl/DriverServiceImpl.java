package com.ruoyi.wuliu.service.impl;

import com.ruoyi.wuliu.service.DriverService;

/**
 * @author chenliang
 * @date 2023/12/17 19:58
 */
@Service
public class DriverServiceImpl implements DriverService {

    @Override
    public String test() {
        return "hello wuliu modoul";
    }
}
